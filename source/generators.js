var generators = {
  "czech-adjective": {
    'label': 'česká přídavná jména',
    'input_fields': [
      'mužský rod, jednotné číslo, 1. pád 1. stupně přídavného jména ("mladý", "jarní", "rozmarný" atd.)'
    ],
    "forms": {
      "mladý": [
        "s 1  ý$",
        "s 1 ější [bdntpv]ý$ 1 ejší [fghklszm]ý$ 2 řejší rý$",
        "s 1 í [bflmpsvzkrdtn]ý$ 2 zí [^c]hý$ 1 í chý$"
      ],
      "jarní": [
        "s 1 í í$",
        "s 1 ější [bmpvn]í$ 1 ejší [žščřcjďťňflsz]í$"
      ]
    },
    'patterns': {
      'mladý': [
        '/1ý',
        '/1ého',
        '/1ému',
        '/1ého',
        '/1ý',
        '/1ém',
        '/1ým',
        '/1ý',
        '/1ého',
        '/1ému',
        '/1ý',
        '/1ý',
        '/1ém',
        '/1ým',
        '/1á',
        '/1é',
        '/1é',
        '/1ou',
        '/1á',
        '/1é',
        '/1ou', // instrumental, feminine, singular, positive
        '/1é',
        '/1ého',
        '/1ému',
        '/1é',
        '/1é',
        '/1ém',
        '/1ým',
        '/3',
        '/1ých',
        '/1ým',
        '/1é',
        '/3',
        '/1ých',
        '/1ými',
        '/1é',
        '/1ých',
        '/1ým',
        '/1é',
        '/1é',
        '/1ých',
        '/1ými', // instrumental, inanimate masculine, plural, positive
        '/1é',
        '/1ých',
        '/1ým',
        '/1é',
        '/1é',
        '/1ých',
        '/1ými',
        '/1á',
        '/1ých',
        '/1ým',
        '/1á',
        '/1á',
        '/1ých',
        '/1ými',
        '/2', // nominative, animate masculine, singular, comparative
        '/2ho',
        '/2mu',
        '/2ho',
        '/2',
        '/2m',
        '/2m',
        '/2',
        '/2ho',
        '/2mu',
        '/2',
        '/2',
        '/2m',
        '/2m',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2', // instrumental, feminine, singular, comparative
        '/2',
        '/2ho',
        '/2mu',
        '/2',
        '/2',
        '/2m',
        '/2m',
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi', // instrumental, animate masculine, plural, comparative
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi',
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi',
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi',
        'nej/2', // nominative, animate masculine, singular, superlative
        'nej/2ho',
        'nej/2mu',
        'nej/2ho',
        'nej/2',
        'nej/2m',
        'nej/2m',
        'nej/2',
        'nej/2ho',
        'nej/2mu',
        'nej/2',
        'nej/2',
        'nej/2m',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2', // instrumental, feminine, singular, superlative
        'nej/2',
        'nej/2ho',
        'nej/2mu',
        'nej/2',
        'nej/2',
        'nej/2m',
        'nej/2m',
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi', // instrumental, animate masculine, plural, superlative
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi',
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi',
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi'
      ],
      'jarní': [
        '/1',
        '/1ho',
        '/1mu',
        '/1ho',
        '/1',
        '/1m',
        '/1m',
        '/1',
        '/1ho',
        '/1mu',
        '/1',
        '/1',
        '/1m',
        '/1m',
        '/1',
        '/1',
        '/1',
        '/1',
        '/1',
        '/1',
        '/1',
        '/1',
        '/1ho',
        '/1mu',
        '/1',
        '/1',
        '/1m',
        '/1m',
        '/1', // first plural
        '/1ch',
        '/1m',
        '/1',
        '/1',
        '/1ch',
        '/1mi',
        '/1',
        '/1ch',
        '/1m',
        '/1',
        '/1',
        '/1ch',
        '/1mi',
        '/1',
        '/1ch',
        '/1m',
        '/1',
        '/1',
        '/1ch',
        '/1mi',
        '/1',
        '/1ch',
        '/1m',
        '/1',
        '/1',
        '/1ch',
        '/1mi', //last plural
        '/2',
        '/2ho',
        '/2mu',
        '/2ho',
        '/2',
        '/2m',
        '/2m',
        '/2',
        '/2ho',
        '/2mu',
        '/2',
        '/2',
        '/2m',
        '/2m',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2ho',
        '/2mu',
        '/2',
        '/2',
        '/2m',
        '/2m', // last singular comparative
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi',
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi',
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi',
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi', // last plural comparative
        'nej/2',
        'nej/2ho',
        'nej/2mu',
        'nej/2ho',
        'nej/2',
        'nej/2m',
        'nej/2m',
        'nej/2',
        'nej/2ho',
        'nej/2mu',
        'nej/2',
        'nej/2',
        'nej/2m',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2ho',
        'nej/2mu',
        'nej/2',
        'nej/2',
        'nej/2m',
        'nej/2m', // last singular superlative
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi',
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi',
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi',
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi', // last plural superlative
      ]
    }
  },
  "czech-noun-masculine-inanimate": {
    'label': "česká podstatná jména mužská neživotná",
    'input fields': [
      '1. pád čísla jednotného'
    ],
    'forms': {
      'aktivismus': [
        "s 2  smus$"
      ],
      'stroj': [
        's 0  $',
        's 0  [^c]$ 2 c c$'
      ]
    },
    'patterns': {
      'aktivismus': [
        "/1us",
        "/1u",
        "/1u",
        "/1us",
        "/1e",
        "/1u",
        "/1em",
        "/1y",
        "/1ů",
        "/1ům",
        "/1y",
        "/1y",
        "/1ech",
        "/1y"
      ],
      'stroj': [
        '/1',
        '/2e',
        '/2i',
        '/1',
        '/2i',
        '/2i',
        '/2em',
        '/2e',
        '/2ů',
        '/2ům',
        '/2e',
        '/2e',
        '/2ích',
        '/2i'
      ]
    }
  }
};
