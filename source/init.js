const wordCategorySelector = document.getElementById("wordCategorySelector");
const patternSelector = document.getElementById("patternSelector");
const UIForm = document.getElementById("UIForm");
let wordCategory = "";
let pattern = "";
Object.keys(generators).forEach(key => {
  const opt = document.createElement('option');
  opt.value = key;
  opt.innerText = generators[key].label;
  wordCategorySelector.appendChild(opt);
});

const fillPatterns = () => {
  wordCategory = wordCategorySelector.options[wordCategorySelector.selectedIndex].value;
  patternSelector.innerHTML = "";
  Object.keys(generators[wordCategory].patterns).forEach(key => {
    const opt = document.createElement('option');
    opt.value = key;
    opt.innerText = key;
    patternSelector.appendChild(opt);
  });
};

const createUIForm = () => {
  pattern = patternSelector.options[patternSelector.selectedIndex].value;
  UIForm.innerHTML = "";
  generators[wordCategory].input_fields.forEach((pat, idx) => {
    const lbl = document.createElement("label");
    lbl.for = "pattern-" + idx;
    lbl.innerText = pat;
    UIForm.appendChild(lbl);
    const inp = document.createElement("input");
    inp.type = "text";
    inp.id = "pattern-" + (idx + 1);
    UIForm.appendChild(inp);
    UIForm.appendChild(document.createElement("br"));
  });
  const submit = document.createElement("input");
  submit.type = "button";
  submit.onclick = generateFormsAndSubmit;
  submit.value = "Generovat";
  UIForm.appendChild(submit);
};

const generateFormsAndSubmit = () => {
  const regex = /^(\D*)\/(\d+)(\D*)$/;
  const HTMLForm = document.createElement("form");
  HTMLForm.action = "https://tools.wmflabs.org/lexeme-forms/template/" + wordCategory;
  HTMLForm.method = "post";
  generators[wordCategory].forms[pattern].forEach((pat, idx) => {
    const patParts = pat.split(" ");
    if (patParts[0] === "s") {
      let regexes = [];
      for (let i = 3; i < patParts.length; i += 3) {
        regexes.push(new RegExp(patParts[i]));
      }
      regexes.forEach((r, ix) => {
        if (document.getElementById("pattern-1").value.match(r) != null) {
          let inp = document.createElement("input");
          inp.id = "pattern2-" + (idx + 1);
          inp.value = document.getElementById("pattern-1").value.slice(0, -parseInt(patParts[3 * ix + 1])) + patParts[3 * ix + 2];
          document.body.appendChild(inp);
        }
      })
    }
  });
  generators[wordCategory].patterns[pattern].forEach(pat => {
    const match = pat.match(regex);
    const form = match === null ? "" : match[1] + document.getElementById("pattern2-" + match[2]).value + match[3];
    const inp = document.createElement("input");
    inp.type = "hidden";
    inp.name = "form_representation";
    inp.value = form;
    HTMLForm.appendChild(inp);
  });
  document.body.appendChild(HTMLForm);
  HTMLForm.submit();
};