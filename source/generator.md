---
title: Nástroj na generování tvarů slov
date: 2019-11-12 20:05:15
---
{% raw %}
<main class="container mt-3">
  <label for="wordCategorySelector">Zvolte si slovní druh: </label>
  <select onchange="fillPatterns()" id="wordCategorySelector"><option hidden selected disabled value></option></select><br>
  <label for="patternSelector">Zvolte si vzor skloňování slov: </label>
  <select onchange="createUIForm()" id="patternSelector"><option hidden selected disabled value></option></select>
  <form id="UIForm"></form>
</main>
<script src="./generators.js"></script>
<script src="./init.js"></script>
{% endraw %}